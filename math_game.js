// Gloabal Variables
var playing = false;
var score;
var timeremaining;
var countdown;
var correctAns;


// Register Events
document.getElementById("startreset").addEventListener("click", play);
document.getElementById("box1").addEventListener("click", checkAnswer);
document.getElementById("box2").addEventListener("click", checkAnswer);
document.getElementById("box3").addEventListener("click", checkAnswer);
document.getElementById("box4").addEventListener("click", checkAnswer);


function play(e) {
    if(playing === true) {
        // You want to reset
        window.location.reload();
    } else {
        // You want to start the play

        playing = true;

        this.innerHTML = "Reset Game";

        score = 0;
        setText("scoreValue", score);

        hide("gameover");

        timeremaining = 60;
        setText("timeremainingValue", timeremaining);
        show("timeremaining");

        startCountdown();

        generateQA();
    }
}

function generateQA() {
    var x = (1 + Math.round(Math.random() * 9));
    var y = (1 + Math.round(Math.random() * 9));

    correctAns = x * y;

    let quesString = `${x} x ${y}`;
    setText("question", quesString);

    var correctPos = (1 + Math.round(Math.random() * 3));
    setText(`box${correctPos}`, correctAns);

    var answers = [correctAns];
    for(i=1; i<5; i++){
        var wrongAns;

        if(i != correctPos){
            do{
                wrongAns = (1 + Math.round(Math.random() * 9)) * (1 + Math.round(Math.random() * 9));
            } while(answers.indexOf(wrongAns) != -1);

            setText(`box${i}`, wrongAns);
            answers.push(wrongAns);
        }
    }
}

function checkAnswer() {
    if(playing === true) {
        if(this.innerHTML == correctAns) {
            score++;
            setText("scoreValue", score);
            show("correct");
            hide("wrong");
            setTimeout(function(){
                hide("correct");
            }, 500);
            generateQA();
        } else {
            show("wrong");
            hide("correct");
            setTimeout(function(){
                hide("wrong");
            }, 500);
        }
    }
}

function startCountdown() {
    countdown = setInterval(function() {
        timeremaining -= 1;
        setText("timeremainingValue", timeremaining);

        if(timeremaining <= 0) {
            clearInterval(countdown);
            playing = false;
            show("gameover");
            hide("timeremaining");

            setText("scoreValue", "");
            setText("startreset", "Start Game");

            let msg = `<p>Game Over!</p><p>Your Score: ${score}</p>`;
            setText("gameover", msg);
        }
    }, 1000);
}

/* HELPER FUNCTIONS */
function setText(id, text) {
    document.getElementById(id).innerHTML = text;
}
function show(id) {
    document.getElementById(id).style.display = 'block';
}
function hide(id) {
    document.getElementById(id).style.display = 'none';
}